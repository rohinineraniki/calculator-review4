import React from "react";

class Calculator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      output: "",
      sum: 0,
      valuesArray: [],
      givenValues: [],
      allOparators: [],
    };
  }

  handleOperations = () => {
    let array1 = [];
    let array2 = [];

    const { output } = this.state;
    console.log(output, "output");
    let valuesArray = output.split("");

    let startIndex = 0;
    valuesArray.forEach((each, index) => {
      if (!Number(each)) {
        console.log("index", startIndex, index);
        let allValues = valuesArray.slice(startIndex, index);

        console.log("check", valuesArray.slice(1, 2));
        console.log(allValues, "allvalues");

        startIndex = index + 1;
        console.log(index, valuesArray.length - 1);

        array1.push(allValues);
        array2.push(each);
      }
      if (index === valuesArray.length - 1 && Number(each)) {
        console.log("in index");
        console.log(startIndex);

        array1.push(valuesArray.slice(startIndex, valuesArray.length));
      }
    });
    console.log("array1", array1);
    console.log("array2", array2);

    let total = Number(array1[0].join(""));
    for (let index = 1; index < array1.length; index++) {
      console.log(index, "indexx");
      console.log("join", array1[index].join(""));

      let symbolIndex = index - 1;

      if (array2[symbolIndex] === "+") {
        total += Number(array1[index].join(""));
        console.log("plus", total);
      }
      if (array2[symbolIndex] === "-") {
        total -= Number(array1[index].join(""));

        console.log("minus", total);
      }
    }
    this.setState({ sum: total });
  };

  handleClick = (event) => {
    const { output } = this.state;

    console.log(event.target.value);
    console.log(output.length);

    let value1 = "";
    let value2 = "";
    let start = 0;

    let count = 0;
    let operator = "";

    if (event.target.value === "C") {
      this.setState({ output: "", sum: 0 });
    } else if (event.target.value === "=") {
      this.setState({ output: output });
    } else {
      this.setState((prevValue) => ({
        output: prevValue.output + event.target.value,
      }));
    }

    if (event.target.value === "=") {
      console.log("if euqal", output.split(""));
      let arraySplitted = output.split("");
      this.setState({ valuesArray: arraySplitted });
      this.handleOperations();
    }
  };

  render() {
    const { output, sum } = this.state;

    return (
      <div className="d-flex flex-column w-50">
        <h1>Calculator</h1>
        <div className="d-flex flex-column">
          <input type="text" value={output} className="form-control" />
          <input type="text" value={sum} className="form-control" />
        </div>
        <div className="keys-container w-50">
          <button
            className="btn btn-primary m-3"
            onClick={this.handleClick}
            value="1"
          >
            1
          </button>
          <button
            className="btn btn-primary m-3"
            value="2"
            onClick={this.handleClick}
          >
            2
          </button>
          <button
            className="btn btn-primary m-3"
            value="3"
            onClick={this.handleClick}
          >
            3
          </button>
          <button
            className="btn btn-primary m-3"
            value="4"
            onClick={this.handleClick}
          >
            4
          </button>
          <button
            className="btn btn-primary m-3"
            value="5"
            onClick={this.handleClick}
          >
            5
          </button>
          <button
            className="btn btn-primary m-3"
            value="6"
            onClick={this.handleClick}
          >
            6
          </button>
          <button
            className="btn btn-primary m-3"
            value="7"
            onClick={this.handleClick}
          >
            7
          </button>
          <button
            className="btn btn-primary m-3"
            value="8"
            onClick={this.handleClick}
          >
            8
          </button>
          <button
            className="btn btn-primary m-3"
            value="9"
            onClick={this.handleClick}
          >
            9
          </button>
          <button
            className="btn btn-primary m-3"
            value="0"
            onClick={this.handleClick}
          >
            0
          </button>
          <button
            className="btn btn-primary m-3"
            value="+"
            onClick={this.handleClick}
          >
            +
          </button>
          <button
            className="btn btn-primary m-3"
            value="-"
            onClick={this.handleClick}
          >
            -
          </button>
          <button
            className="btn btn-primary m-3"
            value="="
            onClick={this.handleClick}
          >
            =
          </button>
          <button
            className="btn btn-primary m-3"
            value="C"
            onClick={this.handleClick}
          >
            C
          </button>
        </div>
      </div>
    );
  }
}

export default Calculator;
